﻿using System;
using System.Collections.Generic;
using System.Numerics;

/*
2. feladat

Írj metódust, mely megadja, hogy n Ft hányféleképpen fizethető ki pontosan
1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000 és 20000 Ft-osokkal
(feltételezve, hogy a két legkisebb címletet még nem vonták ki a forgalomból).
A kifizetésben a különböző címletek sorrendje nem számít.
Pl. n=5 bemenetre 4 a visszaadott érték: 1+1+1+1+1, 2+1+1+1, 2+2+1, 5

A beadandó osztály ne tartalmazzon külső függőséget, és a következő interfészt valósítsa meg:

interface IPaymentCounter
{
   BigInteger GetCountOfProperPayments(int n);
}

Értékkorlátok:
n <= 1000000
*/

namespace F2_Fizetes
{
    class OzsvartKaroly_F2 : IPaymentCounter
    {
        BigInteger NumberOfWays;
        int[] banknotes = new int[] { 1, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000 };
        int[] quantities; //quantities.Length == banknotes.Length ALWAYS
        int payment; //This is the incoming 'n' parameter, the money we pay in a lot of ways

        public BigInteger GetCountOfProperPayments(int n)
        {
            #region Error check and trivial solutions
            if (n > 1000000)
                throw new ArgumentException("Parameter cannot be over 1 000 000");
            if (n <= 0) return BigInteger.Zero;
            if (n == 1) return 1;
            #endregion
            FilterBanknotes(n);
            quantities = new int[banknotes.Length];
            payment = n;

            quantities = new int[banknotes.Length];

            #region Debug on console if needed:
            //for (int i = 0; i < banknotes.Length; i++) Console.Write(banknotes[i] + "\t"); Console.WriteLine();
            //for (int i = 0; i < banknotes.Length; i++) Console.Write("======"); Console.WriteLine(); //2   5   10
            #endregion

            //Do calculation
            Recursive(0);

            return NumberOfWays;
        }

        /// <summary>
        /// Filters the banknotes. Takes out every banknote which is bigger than the number
        /// </summary>
        /// <param name="n"></param>
        private void FilterBanknotes(int number)
        {
            List<int> newNotes = new List<int>();

            for (int i = 0; i < banknotes.Length && banknotes[i] <= number; i++)
            {
                newNotes.Add(banknotes[i]);
            }
            banknotes = newNotes.ToArray();
        }

        private void Recursive(int level)
        {
            if (level >= banknotes.Length)
                return;

            Recursive(level + 1);

            while (true)
            {
                quantities[level]++;
                int paymentValue = GetValue();

                #region If needed: PRINT TO CONSOLE

                //if (paymentValue == payment)
                //{
                //    quantities.ToList().ForEach(x => Console.Write(x + "\t"));
                //    Console.Write(paymentValue);
                //    Console.ForegroundColor = ConsoleColor.Green;
                //    Console.Write("\t Correct");
                //    Console.ResetColor();
                //    Console.WriteLine();
                //}
                //else if (paymentValue > payment)
                //{
                //    quantities.ToList().ForEach(x => Console.Write(x + "\t"));
                //    Console.Write(paymentValue);
                //    Console.ForegroundColor = ConsoleColor.Red;
                //    Console.Write("\t Too much");
                //    Console.ResetColor();
                //    Console.WriteLine();
                //}
                //else
                //{
                //    quantities.ToList().ForEach(x => Console.Write(x + "\t"));
                //    Console.Write(paymentValue);
                //    Console.ForegroundColor = ConsoleColor.Blue;
                //    Console.Write("\t Too little");
                //    Console.ResetColor();
                //    Console.WriteLine();
                //}

                #endregion

                if (paymentValue == payment)
                {
                    NumberOfWays++;
                    quantities[level] = 0;
                    return;
                }
                else if (paymentValue > payment)
                {
                    quantities[level] = 0;
                    return;
                }

                Recursive(level + 1);
            }
        }
        private int GetValue()
        {
            int sum = 0;
            for (int i = 0; i < quantities.Length; i++)
            {
                sum += quantities[i] * banknotes[i];
            }
            return sum;
        }

        public OzsvartKaroly_F2()
        {
            NumberOfWays = 0;
        }
    }
    interface IPaymentCounter
    {
        BigInteger GetCountOfProperPayments(int n);
    }
}